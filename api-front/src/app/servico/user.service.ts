import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../modelo/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  //Url da api
  private url:string = 'http://localhost:8081'

  private urlGetAllUsers = 'http://localhost:8081/users'

  private urlNovoUser = 'http://localhost:8081/user'  //para novo e edicao usuario

  constructor(private http:HttpClient) { }


  selecionar():Observable<User[]>{
    return this.http.get<User[]>(this.urlGetAllUsers);
  }

  // Método para cadastrar usuários
  cadastrar(obj:User):Observable<User>{
    return this.http.post<User>(this.urlNovoUser, obj);
  }

  // Método para editar usuários
  editar(obj:User):Observable<User>{
    return this.http.put<User>(this.urlNovoUser, obj);
  }

  // Método para remover usuários
  remover(id:number):Observable<void>{
    return this.http.delete<void>(this.url + '/' + id);
  }

}
