import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { User } from '../modelo/User';
import { UserService } from '../servico/user.service';
import { BrowserModule } from '@angular/platform-browser'

@Component({
  selector: 'app-principal',
  standalone: true,
  imports: [CommonModule,FormsModule], 
  templateUrl: './principal.component.html',
  styleUrl: './principal.component.css'
})
export class PrincipalComponent {

    user = new User();

    //Variável para visibilidade dos botões
    btnCadastro:boolean = true;

    //Variável para visibilidade da tabela
    tabela:boolean = true;

    //Json de usuários
    public usuarios:User[] = [];

    //Construtor
    constructor(private servico: UserService){}

   //Mètodo de listagem
   selecionar():void{
    this.servico.selecionar()
    .subscribe(retorno => this.usuarios = retorno )
   }

   //Método de cadastro
   cadastrar():void{
    this.servico.cadastrar(this.user)
      .subscribe(retorno => { 

        //Cadastrar o usuário no vetor
        this.usuarios.push(retorno); 
        //Limpar o formulário
        this.user = new User();
        //Mensagem
        alert('Usuário cadstrado com sucesso !');
      });
   }

   //Método para selecionar um usuário específicoo
   selecionarUsuario(posicao:number):void{
    //Selecionar usuário no vetor
    this.user = this.usuarios[posicao];
    //Visibilidade dos botões
    this.btnCadastro = false;
    //Visibilidade da tabela
    this.tabela = false;
   }

   //Método para editar um usuário
   editar():void{
    this.servico.editar(this.user)
    .subscribe(retorno => {

      //Obter posição do vetor onde está o user
      let posicao = this.usuarios.findIndex(obj => {
        return obj.id == retorno.id;
      });

      //alterar os dados do usuário no vetor
      this.usuarios[posicao] = retorno;

      //Limpar formulário
      this.user = new User();

      //visibilidade dos botões
      this.btnCadastro = true;

      //visibilidade da tabela
      this.tabela = true;

      //mensagem no front
      alert('Usuário alterado com sucesso !');

    });
   }

   
   //Método de inicialização
   ngOnInit(){
    this.selecionar();
   }


}
