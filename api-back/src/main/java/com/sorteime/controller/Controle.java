package com.sorteime.controller;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sorteime.model.Usuario;
import com.sorteime.repository.UserRepository;

@RestController
@CrossOrigin(origins = "*")
public class Controle {
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/")
	public String teste() {
		return "ola mundo";
	}
	
	@PostMapping("/user")
	public ResponseEntity<Usuario> cadastrar(@RequestBody Usuario user) {
		return new ResponseEntity<Usuario>(userRepository.save(user), HttpStatus.OK) ;
	}
	
	@PutMapping("/user")
	public ResponseEntity<Usuario> editar(@RequestBody Usuario user) {
		Usuario u = userRepository.findById(user.getCodigo() )
		.orElseThrow(() -> new EntityNotFoundException());
		System.out.println("---------usuario do banco----------  " + u);

		u.setNome(user.getNome() );
		u.setEmail(user.getEmail() );
		u.setNumSeguroSocial(user.getNumSeguroSocial() );

		userRepository.save(u);
		return new ResponseEntity<Usuario>(HttpStatus.OK) ;
	}
	
	@DeleteMapping("/{codigo}")
	public void remover(@PathVariable long codigo) {
		userRepository.deleteById(codigo);
	}
	
	@GetMapping("/users")
	public ResponseEntity<List<Usuario>> getAllUser() {
		return new ResponseEntity<List<Usuario>>(userRepository.findAll(), HttpStatus.OK);
	}

}
