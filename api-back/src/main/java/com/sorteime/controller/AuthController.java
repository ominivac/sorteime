package com.sorteime.controller;



import java.util.Collections;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sorteime.dto.AuthResponseDTO;
import com.sorteime.dto.LoginDto;
import com.sorteime.dto.UserDTO;
import com.sorteime.model.Role;
import com.sorteime.model.Usuario;
import com.sorteime.repository.RoleRepository;
import com.sorteime.repository.UserRepository;
import com.sorteime.security.JWTGenerator;



@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;
    
    private JWTGenerator jwtGenerator;

    
    @Autowired
    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository,
            RoleRepository roleRepository, PasswordEncoder passwordEncoder, JWTGenerator jwtGenerator) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtGenerator = jwtGenerator;
    }

    
    @PostMapping("login")
    public ResponseEntity<AuthResponseDTO> login(@RequestBody LoginDto loginDto){
		Authentication authentication =
				authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginDto.getNome(), loginDto.getPassword()));
    	
		SecurityContextHolder.getContext().setAuthentication(authentication);
		//authentication =  SecurityContextHolder.getContext().getAuthentication();
		String token = jwtGenerator.generateToken(authentication);
		
    	//return new ResponseEntity<>("Usuário logado com sucesso!", HttpStatus.OK);
		
		return new ResponseEntity<>(new AuthResponseDTO(token), HttpStatus.OK);
    	
    }
    
    
    
    @PostMapping("register")
    public ResponseEntity<String> register(@RequestBody UserDTO userDTO){
        if(userRepository.existsByNome(userDTO.getNome())){
            return new ResponseEntity<>("usuário ja registrado !", HttpStatus.BAD_REQUEST);
        }

        Usuario user = new Usuario();
        user.setNome(userDTO.getNome() );
        user.setPassword(passwordEncoder.encode( userDTO.getPassword()));
       
        Role roles = roleRepository.findByNome("USER").get();
        user.setRoles(Collections.singletonList(roles));

        userRepository.save(user);

        return new ResponseEntity<>("Usuario registrado com sucesso !", HttpStatus.OK);
    }
    
    @GetMapping("users")
	public ResponseEntity<List<Usuario>> getAllUser() {
		return new ResponseEntity<List<Usuario>>(userRepository.findAll(), HttpStatus.OK);
	}

    
    @DeleteMapping("{codigo}")
	public void remover(@PathVariable long codigo) {
		userRepository.deleteById(codigo);
	}
    
    @PutMapping("user")
	public ResponseEntity<Usuario> editar(@RequestBody Usuario user) {
		Usuario u = userRepository.findById(user.getCodigo() )
		.orElseThrow(() -> new EntityNotFoundException());
		System.out.println("---------usuario do banco----------  " + u);

		u.setNome(user.getNome() );
		u.setEmail(user.getEmail() );
		u.setNumSeguroSocial(user.getNumSeguroSocial() );

		userRepository.save(u);
		return new ResponseEntity<Usuario>(HttpStatus.OK) ;
	}
    
    
    @GetMapping("sortear")
   	public ResponseEntity<Usuario> sortear() {
   		Usuario u = userRepository.selectRandomId();
   		//.orElseThrow(() -> new EntityNotFoundException());
   		System.out.println("---------usuario sorteado ----------  " + u);

   		
   		return new ResponseEntity<Usuario>(userRepository.selectRandomId(), (HttpStatus.OK) ) ;
   	}

    @GetMapping("dados")
   	public ResponseEntity<Usuario> findMyOwmData(@RequestBody UserDTO userDTO) {
    	System.out.println("--------UserDTO ------- "  + userDTO);
   		Usuario u = userRepository.findByNamePassword(userDTO.getNome(), userDTO.getPassword() );
   		//.orElseThrow(() -> new EntityNotFoundException());
   		System.out.println("---------dados usuario logado ----------  " + u);

   		
   		return new ResponseEntity<Usuario>(u, (HttpStatus.OK) ) ;
   	}
    
    
}

