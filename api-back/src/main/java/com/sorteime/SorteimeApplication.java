package com.sorteime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SorteimeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SorteimeApplication.class, args);
	}

}
