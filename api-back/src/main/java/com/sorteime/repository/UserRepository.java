package com.sorteime.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.sorteime.model.Usuario;

@Repository
public interface UserRepository extends JpaRepository<Usuario, Long>{

    void save(Optional<Usuario> u);

    Usuario findByNome(String username);

    Boolean existsByNome(String username);
    
    Usuario selectRandomId();

    @Query(value = "SELECT id , email, nome, num_seguro_social, password FROM user where nome = :nome and password = :password",  nativeQuery = true)
    public Usuario findByNamePassword(@Param("nome") String nome, @Param("password") String password);


}
