package com.sorteime.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sorteime.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

    Optional<Role> findByNome(String name);
    
}
