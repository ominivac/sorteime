package com.sorteime.security;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsPasswordService;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.sorteime.model.Role;
import com.sorteime.model.Usuario;
import com.sorteime.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService{

    private UserRepository userRepository;

    public CustomUserDetailsService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Usuario usuario  = userRepository.findByNome(username);
        usuario = Optional.ofNullable(usuario)
        .orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado"));

       return new User(usuario.getNome(), usuario.getPassword(), mapRolesToAuthorities((List<Role>) usuario.getRoles()));
    }

    public UserDetails findByUserByNamePassword(String name, String password) {
        Usuario usuario  = userRepository.findByNamePassword(name, password);
        usuario = Optional.ofNullable(usuario)
        .orElseThrow(() -> new UsernameNotFoundException("Usuário não encontrado"));

       return new User(usuario.getNome(), usuario.getPassword(), mapRolesToAuthorities((List<Role>) usuario.getRoles()));
    }




    private Collection<GrantedAuthority> mapRolesToAuthorities(List<Role> roles){
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getNome())).collect(Collectors.toList());
    }

   
   
    
}
