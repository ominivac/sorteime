package com.sorteime.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

import lombok.Getter;
import lombok.Setter;

@Entity
@NamedNativeQuery(name = "Usuario.selectRandomId", 
	query = "SELECT id , email, nome, num_seguro_social, password FROM user ORDER BY RAND() LIMIT 1", resultClass = Usuario.class)
@Table(name = "user")
@Getter
@Setter
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	private String nome;

	private String password;
	
	private String numSeguroSocial;
	
	private String email;


	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "user_role",
		joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id")
	)
    private Collection<Role> roles = new ArrayList<Role>();


	public Usuario(){
		
	}

	




    public Usuario(String nome2, String password2, Collection<GrantedAuthority> mapRolesToAuthorities) {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "User [codigo=" + id + ", nome=" + nome + ", numSeguroSocial=" + numSeguroSocial + ", email=" + email
				+ "]";
	}




    public Usuario orElseThrow(Object object) {
        return null;
    }


	public Long getCodigo() {
		return id;
	}
	

	
	

}
