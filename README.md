
Instruções para importação do projeto :

- No mysql criar o db sorteime
- no VS CODE abrir as pastas api-front e api-back em duas janelas
- insert de dois dados na tabela roles : ADMIN  e  USER
- run na api-back
- run na api-front com o comando ng s em um novo terminal apontando para a pasta


Endpoints :

Registrar Novo user - POST 
body, JSON , EX :

{"nome": "Sandra", "email":"sandra@gmail.com", "password": "7748844"}

salvar via postman pois a senha está criptografada


http://localhost:8081/api/auth/register

Login User :
ex :

{"nome": "Sandra", "password": "7748844"}








Editar user - PUT
body, JSON , EX :
{"nome": "Sandra", "email":"sandra@gmail.com", "numSeguroSocial": "7748844"}
http://localhost:8081/user

Delete user - DELETE
body, JSON , EX :
{"nome": "Sandra", "email":"sandra@gmail.com", "numSeguroSocial": "7748844"}
http://localhost:8081/user


Listar todos - GET
http://localhost:8081/users



